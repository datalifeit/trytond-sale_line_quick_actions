# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale


def register():
    Pool.register(
        sale.SaleLine,
        sale.SaleLineQuickActionSplit,
        sale.SaleLineQuickActionMove,
        sale.SaleLineQuickActionDuplicate,
        module='sale_line_quick_actions', type_='model')
    Pool.register(
        sale.SaleLineQuickAction,
        module='sale_line_quick_actions', type_='wizard')
